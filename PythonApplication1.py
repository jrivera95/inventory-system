from PyQt5 import uic, QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QEvent
from PyQt5.QtGui import QCloseEvent
from PyQt5.QtWidgets import *
import myconnutils
import sys
import pymysql
from datetime import date


class MainView(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainView, self).__init__()
        uic.loadUi('mainwindow.ui', self)
        # connect exit button
        self.btnExit.clicked.connect(self.close)

        # instantiate logview window and show it, connect exit button
        self.loginview = LogView()
        self.loginview.show()
        self.loginview.btnExit.clicked.connect(self.close)

        # connect window buttons to window show functions
        self.btnViewInventory.clicked.connect(self.showInventory)
        self.btnNewOrder.clicked.connect(self.showAddOrderView)
        self.btnManageOrders.clicked.connect(self.showManageOrderView)
        self.btnEditEmployee.clicked.connect(self.showEditEmployeeView)
        self.btnPayroll.clicked.connect(self.showPayrollView)
        self.btnViewOrders.clicked.connect(self.showViewOrders)
        self.btnViewPay.clicked.connect(self.showViewPayroll)
        self.btnUsers.clicked.connect(self.showUsers)

    def showUsers(self):
        self.userview = UsersView()
        self.userview.show()

    def showInventory(self):
        self.inventview = InventoryView()
        self.inventview.show()

    def showViewPayroll(self):
        self.payrollview = ViewPayrollView()
        self.payrollview.show()

    def showViewOrders(self):
        self.orderview = ViewOrdersView()
        self.orderview.show()

    def showAddOrderView(self):
        self.addview = AddOrderView()
        self.addview.show()

    def showManageOrderView(self):
        self.manageview = ManageOrderView()
        self.manageview.show()

    def showEditEmployeeView(self):
        self.editemployee = EditEmployeeView()
        self.editemployee.show()

    def showPayrollView(self):
        self.payemployee = PayrollView()
        self.payemployee.show()


class LogView(QtWidgets.QDialog):
    def __init__(self):
        super(LogView, self).__init__()
        uic.loadUi('login.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.btnExit.clicked.connect(self.close)
        self.btnLog.clicked.connect(lambda: self.authenticate())
        #TODO keypress event for escape button closing whole app
        #TODO same thing as above but with red X button

    def authenticate(self):
        # initiate connection to production database
        connection = myconnutils.getprodconn()

        sql = "SELECT Username, Password " \
              "FROM users " \
              "ORDER BY UserID"

        cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()

        rows = cursor.fetchall()

        userEntered = self.txtUser.text()
        passEntered = self.txtPass.text()

        for rowIndex, record in enumerate(rows):
            realUser = record['Username']
            realPass = record['Password']

            if userEntered == realUser and passEntered == realPass:
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Welcome')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('Log in succesful!')
                popup.exec_()
                self.close()
                return

        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Login Failed')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Incorrect Username or Password.')
        popup.exec_()

class UsersView(QtWidgets.QDialog):
    def __init__(self):
        super(UsersView, self).__init__()
        uic.loadUi('users.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('Manage Users')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        self.setWindowIcon(icon)
        self.popUsersTable()
        self.btnClose.clicked.connect(self.close)
        self.btnSave.clicked.connect(self.saveUsers)

    def popUsersTable(self):
        # open connection to production database
        connection = myconnutils.getprodconn()

        sql = "Select * from users ORDER BY UserID"

        try:
            # create cursor and execute sql
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall() # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                rowCount = self.tblUsers.rowCount()
                self.tblUsers.insertRow(rowCount)
                userName = QTableWidgetItem(record['Username'])
                userPass = QTableWidgetItem(str(record['Password']))

                self.tblUsers.setItem(rowIndex, 0, userName)  # positioning cell
                self.tblUsers.setItem(rowIndex, 1, userPass)  # positioning cell

        finally:
            # close connection
            connection.close()

    def saveUsers(self):

        # iterar por valores de tabla hasta que encuentra un campo vacío
        allRows = self.tblUsers.rowCount()
        allColumns = self.tblUsers.columnCount()

        for row in range(allRows):
            valuesList = []
            valuesList.append(row+1)
            if self.tblUsers.item(row, 0) is not None:
                for column in range(allColumns):
                    if self.tblUsers.item(row, column) is not None:
                        cellContent = self.tblUsers.item(row, column).text()
                        valuesList.append(cellContent)

                    else:
                        break

                try:
                    sql = "UPDATE users SET Username='{1}', Password='{2}' " \
                          "WHERE user.UserID='{0}'".format(*valuesList)

                    sql1 = "INSERT INTO users VALUES ({0}, '{1}', '{2}') " \
                           "ON DUPLICATE KEY UPDATE Username='{1}', Password='{2}'".format(*valuesList)

                except IndexError as error:
                    # shows message box about error
                    popup = QtWidgets.QMessageBox()
                    popup.setWindowTitle('Error')
                    icon = QtGui.QIcon("./img/mangotech.jpg")
                    popup.setWindowIcon(icon)
                    popup.setText('Last row not saved. Please enter a value '
                                  'in every field when entering a new record.')
                    popup.exec_()
                    return

                else:
                    if cellContent == '':
                        # shows message box about error
                        popup = QtWidgets.QMessageBox()
                        popup.setWindowTitle('Error')
                        icon = QtGui.QIcon("./img/mangotech.jpg")
                        popup.setWindowIcon(icon)
                        popup.setText(
                            'Please do not leave any fields blank when entering a new record.')
                        popup.exec_()
                        break

                try:
                    # create cursor and execute sql to save inventory table
                    connection = myconnutils.getprodconn()
                    cursor = connection.cursor()
                    cursor.execute(sql1)
                    connection.commit()

                finally:
                    # close connection
                    connection.close()

                try:
                    # create cursor and execute sql to save inventory table
                    connection = myconnutils.getprodconn()
                    cursor = connection.cursor()
                    # cursor.execute(sql)
                    connection.commit()
                finally:
                    # close connection
                    connection.close()

        insertPosition = self.tblUsers.rowCount()
        self.tblUsers.insertRow(insertPosition)

        # shows message box saying inventory was succesfully udpated
        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Manage Users')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('User List Updated!')
        popup.exec_()


class ViewPayrollView(QtWidgets.QDialog):
    def __init__(self):
        super(ViewPayrollView, self).__init__()
        uic.loadUi('viewpayroll.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('View Payroll')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        self.setWindowIcon(icon)
        self.popPayrollTable()
        self.btnClose.clicked.connect(self.close)

    def popPayrollTable(self):
        # open connection to payroll database
        connection = myconnutils.getpayconn()
        sql = "select employee.*, department.DepartmentName, salary.* " \
              "from employee " \
              "inner join department on employee.DepartmentID = department.DepartmentID " \
              "left join salary on employee.EmployeeID = salary.EmployeeID " \
              "ORDER BY EmployeeName;"

        try:
            # create a cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                self.tblPayTable.insertRow(rowIndex)
                empFirst = QTableWidgetItem(record['EmployeeName']).text()
                empLast = QTableWidgetItem(record['EmployeeLastName']).text()
                empName = QTableWidgetItem(empFirst + ' ' + empLast)
                empDept = QTableWidgetItem(record['DepartmentName'])
                empPos = QTableWidgetItem(record['EmployeePosition'])

                strHours = str(record['HoursWorkedInPeriod'])
                empHours = QTableWidgetItem(strHours)

                strTotal = str(record['TotalSalary'])
                empTotal = QTableWidgetItem(strTotal)

                strDate = str(record['LastPaymentDate'])

                if strDate == 'None':
                    empLastPay = QTableWidgetItem(strDate)
                else:
                    empLastPay = QTableWidgetItem(strDate)

                strRate = str(record['HourlyRate'])
                empRate = QTableWidgetItem(strRate)

                self.tblPayTable.setItem(rowIndex, 0, empName)
                self.tblPayTable.setItem(rowIndex, 1, empDept)
                self.tblPayTable.setItem(rowIndex, 2, empPos)
                self.tblPayTable.setItem(rowIndex, 3, empRate)
                self.tblPayTable.setItem(rowIndex, 4, empHours)
                self.tblPayTable.setItem(rowIndex, 5, empTotal)
                self.tblPayTable.setItem(rowIndex, 6, empLastPay)

        finally:
            # close connection
            connection.close()


class ViewOrdersView(QtWidgets.QDialog):
    def __init__(self):
        super(ViewOrdersView, self).__init__()
        uic.loadUi('vieworders.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('View Orders')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        self.setWindowIcon(icon)
        self.popOrderTable()
        self.btnClose.clicked.connect(self.close)

    def popOrderTable(self):
        # open connection to prod database
        connection = myconnutils.getprodconn()

        sql = "SELECT `order`.*, client.ClientFirstName, client.ClientLastName, orderpayment.* " \
              "FROM `order` " \
              " inner join client on `order`.ClientID = client.ClientID " \
              " inner join orderpayment on `order`.OrderID = orderpayment.OrderID " \
              "ORDER BY `order`.OrderID"

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            for rowIndex, record in enumerate(rows):
                fName = record['ClientFirstName']
                lName = record['ClientLastName']
                clientName = QTableWidgetItem(fName + ' ' + lName)
                priceTotal = QTableWidgetItem(str(record['OrderPriceTotal']))

                lastPayment = record['LastPaymentDate']
                strLastPayment = lastPayment.strftime("%Y-%m-%d")
                qtLastPayment = QTableWidgetItem(strLastPayment)

                totalDeposit = QTableWidgetItem(str(record['Deposit']))

                amtDue = QTableWidgetItem(str(record['AmountDue']))

                placedDate = record['OrderPlacedDate']
                if placedDate is None or placedDate == 'None':
                    qtPlacedDate = QTableWidgetItem(str(placedDate))
                else:
                    strPlacedDate = placedDate.strftime("%Y-%m-%d")
                    qtPlacedDate = QTableWidgetItem(strPlacedDate)

                compDate = record['OrderDateCompleted']
                if compDate is None or compDate == 'None':
                    qtcompDate = QTableWidgetItem(str(compDate))

                else:
                    strcompDate = compDate.strftime("%Y-%m-%d")
                    qtcompDate = QTableWidgetItem(strcompDate)

                insDate = record['OrderInstallationDate']
                if insDate is None or insDate == 'None':
                    qtinsDate = QTableWidgetItem(str(insDate))

                else:
                    strinsDate = insDate.strftime("%Y-%m-%d")
                    qtinsDate = QTableWidgetItem(strinsDate)

                insertPosition = self.tblOrderTable.rowCount()
                self.tblOrderTable.insertRow(insertPosition)

                self.tblOrderTable.setItem(rowIndex, 0, clientName)
                self.tblOrderTable.setItem(rowIndex, 1, priceTotal)
                self.tblOrderTable.setItem(rowIndex, 2, qtLastPayment)
                self.tblOrderTable.setItem(rowIndex, 3, totalDeposit)
                self.tblOrderTable.setItem(rowIndex, 4, amtDue)
                self.tblOrderTable.setItem(rowIndex, 5, qtPlacedDate)
                self.tblOrderTable.setItem(rowIndex, 6, qtcompDate)
                self.tblOrderTable.setItem(rowIndex, 7, qtinsDate)


        finally:
            connection.close()


class InventoryView(QtWidgets.QDialog):
    def __init__(self):
        super(InventoryView, self).__init__()
        uic.loadUi('inventario.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('Manage Inventory')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        self.setWindowIcon(icon)
        self.popInvTable()
        self.btnClose.clicked.connect(self.close)
        self.btnSave.clicked.connect(self.saveInv)

    # para poblar tabla de inventario
    def popInvTable(self):
        # open connection to production database
        connection = myconnutils.getprodconn()

        sql = "Select * from Inventory ORDER BY InventoryID"

        try:
            # create cursor and execute sql
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall() # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                rowCount = self.tblInvTable.rowCount()
                self.tblInvTable.insertRow(rowCount)
                itemDesc = QTableWidgetItem(record['InventoryDescription'])
                itemStock = QTableWidgetItem(str(record['InventoryStock']))
                itemCost = QTableWidgetItem(record['InventoryCost'])

                self.tblInvTable.setItem(rowIndex, 0, itemDesc)  # positioning cell
                self.tblInvTable.setItem(rowIndex, 1, itemStock)  # positioning cell
                self.tblInvTable.setItem(rowIndex, 2, itemCost)  # positioning cell

        finally:
            # close connection
            connection.close()

    # para guardar/actualizar tabla de inventario
    def saveInv(self):
        # open connection to production database
        connection = myconnutils.getprodconn()

        # do query to fetch inventory table column names
        sql = "SELECT * FROM inventory WHERE 1=0;"

        # create cursor and execute sql
        cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()

        rows = cursor.fetchall()  # this is your list of dictionaries

        colList = []
        # populate list with column titles
        for i in range(1, 4):
            desc = cursor.description[i]
            colList.append(desc[0])

        # iterar por valores de tabla hasta que encuentra un campo vacío
        allRows = self.tblInvTable.rowCount()
        allColumns = self.tblInvTable.columnCount()

        for row in range(allRows):
            valuesList = []
            valuesList.append(row+1)
            if self.tblInvTable.item(row, 0) is not None:
                for column in range(allColumns):
                    if self.tblInvTable.item(row, column) is not None:
                        cellContent = self.tblInvTable.item(row, column).text()
                        valuesList.append(cellContent)

                    else:
                        break

                try:
                    sql = "UPDATE inventory SET InventoryDescription='{1}', InventoryStock='{2}', InventoryCost='{3}' " \
                          "WHERE inventory.InventoryID='{0}'".format(*valuesList)

                    sql1 = "INSERT INTO inventory VALUES ({0}, '{1}', '{2}', '{3}') " \
                           "ON DUPLICATE KEY UPDATE InventoryDescription='{1}', InventoryStock='{2}', InventoryCost='{3}' ".format(*valuesList)

                except IndexError as error:
                    # shows message box about error
                    popup = QtWidgets.QMessageBox()
                    popup.setWindowTitle('Error')
                    icon = QtGui.QIcon("./img/mangotech.jpg")
                    popup.setWindowIcon(icon)
                    popup.setText('Last row not saved. Please enter a value '
                                  'in every field when entering a new record.')
                    popup.exec_()
                    return

                else:
                    if cellContent == '':
                        # shows message box about error
                        popup = QtWidgets.QMessageBox()
                        popup.setWindowTitle('Error')
                        icon = QtGui.QIcon("./img/mangotech.jpg")
                        popup.setWindowIcon(icon)
                        popup.setText(
                            'Please do not leave any fields blank when entering a new record.')
                        popup.exec_()
                        break

                try:
                    # create cursor and execute sql to save inventory table
                    connection = myconnutils.getprodconn()
                    cursor = connection.cursor()
                    cursor.execute(sql1)
                    connection.commit()

                finally:
                    # close connection
                    connection.close()

                try:
                    # create cursor and execute sql to save inventory table
                    connection = myconnutils.getprodconn()
                    cursor = connection.cursor()
                    # cursor.execute(sql)
                    connection.commit()
                finally:
                    # close connection
                    connection.close()

        insertPosition = self.tblInvTable.rowCount()
        self.tblInvTable.insertRow(insertPosition)

        # shows message box saying inventory was succesfully udpated
        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Manage Inventory')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Inventory Succesfully Updated!')
        popup.exec_()


class AddOrderView(QtWidgets.QDialog):
    def __init__(self):
        super(AddOrderView, self).__init__()
        uic.loadUi('createorder.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('Add New Order')
        self.populateCityComboBox()
        self.populateEmployeeComboBox()
        self.populateClientComboBox()
        self.populateItemComboBox()

        # connect buttons
        self.btnExit.clicked.connect(self.close)
        self.btnAddItem.clicked.connect(self.addInvItem)
        self.btnPlaceOrder.clicked.connect(self.placeOrder)
        self.btnAddEmployee.clicked.connect(self.addEmployee)

    def populateClientComboBox(self):
        # open connection to production database
        connection = myconnutils.getprodconn()
        sql = "select ClientLastName, ClientFirstName from client ORDER BY ClientFirstName"

        try:
            # create cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                clientName = record['ClientFirstName'] + " " + record['ClientLastName']
                self.cbClient.addItem(clientName)

        finally:
            # close connection
            connection.close()

    def populateEmployeeComboBox(self):
        # open connection to payroll database
        connection = myconnutils.getpayconn()
        sql = "select EmployeeName, EmployeeLastName, department.DepartmentName " \
              "from employee " \
                "inner join department on employee.DepartmentID = department.DepartmentID " \
              "ORDER BY EmployeeName"

        try:
            # create cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                employeeName = record['EmployeeName'] + " " + record['EmployeeLastName'] + \
                               " (" + record['DepartmentName'] + ")"
                self.cbEmployee.addItem(employeeName)

        finally:
            # close connection
            connection.close()

    def populateItemComboBox(self):
        # open connection to production database
        connection = myconnutils.getprodconn()
        sql = "select InventoryDescription from inventory ORDER BY InventoryDescription"

        try:
            # create cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                inventoryItem = record['InventoryDescription']
                self.cbItem.addItem(inventoryItem)

        finally:
            # close connection
            connection.close()

    def populateCityComboBox(self):
        # open connection to production database
        connection = myconnutils.getprodconn()
        sql = "select * from City ORDER BY CityName"

        try:
            # create cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                cityName = record['CityName']
                self.cbCity.addItem(cityName)

        finally:
            # close connection
            connection.close()

    def addInvItem(self):
        totalPrice = 0.00
        allRows = self.tblOrderItem.rowCount()
        invItem = self.cbItem.currentText()
        quantity = self.spnQuantity.value()

        if invItem == '':
            # shows message box about error
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText(
                'Please choose an inventory item from the item list.')
            popup.exec_()

        elif quantity == 0:
            # shows message box about error
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText(
                'Please enter a valid product quantity.')
            popup.exec_()

        else:
            # add row to order item table
            rowPosition = self.tblOrderItem.rowCount()
            self.tblOrderItem.insertRow(rowPosition)
            insertPosition = rowPosition
            # open connection to production database
            connection = myconnutils.getprodconn()
            sql = "select InventoryDescription, InventoryCost from inventory where InventoryDescription='{0}'".format(invItem)

            try:
                # create cursor and execute sql
                cursor = connection.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()  # this is your list of dictionaries

                for rowIndex, record in enumerate(rows):
                    itemDesc = QTableWidgetItem(record['InventoryDescription'])
                    fltCost = float(record['InventoryCost'].strip('$')) * quantity
                    itemCost = QTableWidgetItem(str(round(fltCost, 2)))
                    itemQuant = QTableWidgetItem(str(quantity))

                    self.tblOrderItem.setItem(insertPosition, 0, itemDesc)  # positioning cell
                    self.tblOrderItem.setItem(insertPosition, 1, itemCost)  # positioning cell
                    self.tblOrderItem.setItem(insertPosition, 2, itemQuant) # positioning cell

            finally:
                # close connection
                connection.close()

                # add price of items added to item list and display it in
                allRows = self.tblOrderItem.rowCount()
                # total price label
                for row in range(allRows):
                    itemPrice = self.tblOrderItem.item(row, 1).text()

                    itemPrice = itemPrice.replace('$', '')
                    totalPrice += float(itemPrice)
                    round(totalPrice, 2)

                self.lblTotalPrice.setText("{:0.2f}".format(totalPrice))

    def addEmployee(self):
        empName = self.cbEmployee.currentText()

        if empName == '':
            # shows message box about error
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText(
                'Please choose an employee from the item list.')
            popup.exec_()

        else:
            # add row to assigned employee table
            rowPosition = self.tblAssigned.rowCount()
            self.tblAssigned.insertRow(rowPosition)
            insertPosition = rowPosition

            empNameSplit = empName.split()
            # open connection to payroll database
            connection = myconnutils.getpayconn()
            sql = "SELECT department.DepartmentName \
                    FROM department \
                    JOIN employee ON department.DepartmentID = employee.DepartmentID \
                    WHERE employee.EmployeeName='{0}' AND employee.EmployeeLastName='{1}'".format(empNameSplit[0], empNameSplit[1])


            try:
                # create cursor and execute sql
                cursor = connection.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()  # this is your list of dictionaries

                for rowIndex, record in enumerate(rows):
                    empName = QTableWidgetItem(empName)
                    empDept = QTableWidgetItem(record['DepartmentName'])

                    self.tblAssigned.setItem(insertPosition, 0, empName)
                    self.tblAssigned.setItem(insertPosition, 1, empDept)


            finally:
                # close connection
                connection.close()

    def placeOrder(self):

        # check for empty fields
        clientBox = self.cbClient.currentText()
        clientFirst = self.lnFirst.text()
        clientLast = self.lnLast.text()
        clientPhone = self.lnPhone.text()
        clientAddress = self.lnAddress.text()
        clientCity = self.cbCity.currentText()
        clientZip = self.lnZipcode.text()
        orderDeposit = self.lnDeposit.text()
        if (not clientBox != '') and (clientFirst == '' or clientLast == '' or clientPhone == '' or clientAddress == '' or clientCity == '' or clientZip == ''):
            # shows message box for first name
            popup1 = QtWidgets.QMessageBox()
            popup1.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup1.setWindowIcon(icon)
            popup1.setText('Please choose a client from the drop down or enter complete information for a new client.')
            popup1.exec_()

        elif self.tblOrderItem.item(0, 0) is None:
            # shows message box for item list
            popup7 = QtWidgets.QMessageBox()
            popup7.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup7.setWindowIcon(icon)
            popup7.setText('Please add at least one item to the item list.')
            popup7.exec_()

        elif orderDeposit == '':
            # shows message box for deposit
            popup6 = QtWidgets.QMessageBox()
            popup6.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup6.setWindowIcon(icon)
            popup6.setText('Please enter a valid deposit amount.')
            popup6.exec_()

        elif self.tblAssigned.item(2, 1) is None:
            # shows message box for assigned employees
            popup8 = QtWidgets.QMessageBox()
            popup8.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup8.setWindowIcon(icon)
            popup8.setText('Please assign at least three employees.')
            popup8.exec_()

        else:
            if clientBox == '':
                clientID = self.SaveClient()

            else:
                fullClientName = clientBox.split()
                clientID = self.getClientID(fullClientName)


            # orderitem table elements
            itemIDs, allItems = self.getItemIDs()
            error = self.saveOrderItems(itemIDs, allItems)
            if error is not None:
                return
            # order table elements
            ordertotalprice = self.lblTotalPrice.text()
            today = date.today()
            orderplaceddate = today.strftime("%Y-%m-%d")
            orderElements = [clientID, ordertotalprice, orderplaceddate]
            self.saveOrder(orderElements)

            # orderpayment table elements
            deposit = self.lnDeposit.text()

            try:
                if deposit == '' or float(deposit) == 0.0:
                    popup = QtWidgets.QMessageBox()
                    popup.setWindowTitle('Edit Employee List')
                    icon = QtGui.QIcon("./img/mangotech.jpg")
                    popup.setWindowIcon(icon)
                    popup.setText('Please a valid deposit amount.')
                    popup.exec_()
                    return

            except ValueError as error:
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Edit Employee List')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('Please a valid deposit amount.')
                popup.exec_()
                return

            amountDue = float(ordertotalprice.strip('$')) - float(deposit.strip('$'))
            lastPaymentDate = orderplaceddate
            self.savePayment(deposit, amountDue, lastPaymentDate)

            # assignedemployee table elements
            self.saveAssignedEmployees()

            # shows message box saying order was succesfully created
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Add New Order')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Order succesfully created!')
            popup.exec_()

    def saveAssignedEmployees(self):
        # open connection to production database
        connection = myconnutils.getprodconn()

        # get most recent order ID
        sql = "SELECT OrderID FROM `order` ORDER BY OrderID DESC LIMIT 1"

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            rowdict = rows[0]
            orderID: int = rowdict['OrderID']

        finally:
            connection.close()

        empRows = self.tblAssigned.rowCount()
        empColumns = self.tblAssigned.columnCount()

        for row in range(empRows):
            empDetail = []
            for column in range(empColumns):
                if self.tblAssigned.item(row, column) is not None:
                    cellContent = self.tblAssigned.item(row, column).text()

                else:
                    continue

                empDetail.append(cellContent)

            strempName = empDetail[0]
            lstempName = strempName.split()
            lstempName.pop(2)
            fname = lstempName[0]
            lname = lstempName[1]
            empDetail.insert(0, lname)
            empDetail.insert(0, fname)
            empDetail.pop(2)

            empDetail.insert(0, orderID)
            # open connection to payroll database
            connection = myconnutils.getprodconn()

            sql = "INSERT INTO assignedemployee (OrderID, EmployeeLastName, EmployeeFirstName, DepartmentID) VALUES ('{0}', '{2}', '{1}', (SELECT DepartmentID from department WHERE DepartmentName='{3}'))".format(*empDetail)

            try:
                cursor = connection.cursor()
                cursor.execute(sql)
                connection.commit()

            finally:
                connection.close()

    def savePayment(self, deposit, amountDue, lastPaymentDate):
        try:
            if deposit == '' or float(deposit) == 0.0:
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Edit Employee List')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('Please a valid deposit amount.')
                popup.exec_()
                return

        except ValueError as error:
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please a valid deposit amount.')
            popup.exec_()
            return

        # open connection to production database
        connection = myconnutils.getprodconn()

        # get most recent order ID
        sql = "SELECT OrderID FROM `order` ORDER BY OrderID DESC LIMIT 1"

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            rowdict = rows[0]
            orderID: int = rowdict['OrderID']

        finally:
            connection.close()

        roundedAmountDue = round(amountDue, 2)
        roundedDeposit = round(float(deposit), 2)

        valuesList = [orderID, roundedAmountDue, roundedDeposit, lastPaymentDate]
        #initialize production database connection
        connection = myconnutils.getprodconn()

        sql = "INSERT INTO orderpayment (OrderID, AmountDue, Deposit, LastPaymentDate) VALUES ('{0}', '{1}', '{2}', '{3}')".format(*valuesList)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

        finally:
            connection.close()

    def saveOrderItems(self, itemIDs, allItems):

        # open connection to production database
        connection = myconnutils.getprodconn()

        # get most recent order ID
        sql = "SELECT OrderID FROM `order` ORDER BY OrderID DESC LIMIT 1"

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            rowdict = rows[0]
            orderID: int = rowdict['OrderID']

        finally:
            connection.close()


        for row in range(len(itemIDs)):
            currentItem = allItems[row]
            sql = "INSERT INTO orderitem (OrderID, InventoryID, ItemQuantity, OrderItemPrice) VALUES ("+str(orderID)+", "+str(itemIDs[row])+", "+str(currentItem[2])+", "+str(currentItem[1])+")"

            sql1 = "UPDATE inventory SET InventoryStock = InventoryStock - "+currentItem[2]+" WHERE InventoryID = "+str(itemIDs[row])+" "
            # initiate connection to production database
            connection = myconnutils.getprodconn()
            try:
                cursor = connection.cursor()
                cursor.execute(sql1)
                cursor.execute(sql)
                connection.commit()

            except pymysql.err.InternalError as error:
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Add Order')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('You used a quantity of an item that is larger than what you have in stock for it.')
                popup.exec_()
                return error

            finally:
                connection.close()

    def saveOrder(self, orderElements):
        # initiate connection to production database
        connection = myconnutils.getprodconn()

        sql = "INSERT INTO `order` (ClientID, OrderPriceTotal, OrderPlacedDate) VALUES ('{0}', '{1}', '{2}')".format(*orderElements)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

        finally:
            connection.close()

    def getItemIDs(self):
        # save order items
        itemRows = self.tblOrderItem.rowCount()
        itemColumns = self.tblOrderItem.columnCount()
        itemIDs = []
        allItems = []

        for row in range(itemRows):
            itemList = []
            for column in range(itemColumns):
                if self.tblOrderItem.item(row, column) is not None:
                    cellContent = self.tblOrderItem.item(row, column).text()

                else:
                    continue

                itemList.append(cellContent)

            allItems.append(itemList)

            # open connection to production database
            connection = myconnutils.getprodconn()
            cursor = connection.cursor()

            sql = "SELECT InventoryID from inventory WHERE InventoryDescription='{0}'".format(*itemList)

            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()
            rowdict = rows[0]
            itemID = rowdict['InventoryID']
            itemIDs.append(itemID)

            connection.close()

        return itemIDs, allItems

    def getClientID(self, fullname):
        # initiate connection to production database
        connection = myconnutils.getprodconn()
        sql = "SELECT ClientID FROM client WHERE ClientLastName='{1}' AND ClientFirstName='{0}'".format(*fullname)

        try:
            # create cursor and execute sql
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()

            rowdict = rows[0]
            clientID: int = rowdict['ClientID']

        finally:
            connection.close()

        return clientID

    def SaveClient(self):
        fname = self.lnFirst.text()
        lname = self.lnLast.text()
        phone = self.lnPhone.text()
        address = self.lnAddress.text()
        cityName = self.cbCity.currentText()
        zip = self.lnZipcode.text()

        clientInfo = [fname, lname, phone, address, cityName, zip]

        # initiate connection to production database
        connection = myconnutils.getprodconn()

        sql = "INSERT INTO client (ClientLastName, ClientFirstName, ClientPhoneNumber, ClientAddress, CityID, ClientZipCode) VALUES ('{1}', '{0}', '{2}', '{3}', (SELECT CityID from city WHERE CityName = '{4}'), '{5}')".format(*clientInfo)
        sql1 = "SELECT ClientID FROM client WHERE ClientLastName='{1}' AND ClientFirstName='{0}'".format(*clientInfo)

        try:
            # create cursor and execute SQL to save client
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

        finally:
            connection.close()

        try:
            # initiate connection to production database
            connection = myconnutils.getprodconn()
            cursor = connection.cursor()
            cursor.execute(sql1)
            connection.commit()

            rows = cursor.fetchall()

            rowdict = rows[0]
            clientID: int = rowdict['ClientID']

        finally:
            connection.close()

        return clientID


class ManageOrderView(QtWidgets.QDialog):
    def __init__(self):
        super(ManageOrderView, self).__init__()
        uic.loadUi('manageorder.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('Manage Orders')
        self.popOrderComboBox()
        self.btnCancel.clicked.connect(self.close)
        self.btnSave.clicked.connect(self.SaveDeposit)
        self.btnCompleted.clicked.connect(self.addCompletedDate)
        self.btnInstalled.clicked.connect(self.addInstalledDate)
        self.cbOrder.activated.connect(self.LoadOrder)

    def popOrderComboBox(self):
        # open connection
        connection = myconnutils.getprodconn()

        sql = "SELECT `order`.OrderID, orderpayment.* " \
              "from `order` " \
                "inner join orderpayment ON `order`.OrderID = orderpayment.OrderID " \
              "WHERE AmountDue <> 0 OR OrderDateCompleted IS NULL OR OrderInstallationDate IS NULL " \
              "ORDER BY `order`.OrderID"

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            for rowIndex, record in enumerate(rows):
                orderID = record['OrderID']
                self.cbOrder.addItem(str(orderID))

        finally:
            #close connection
            connection.close()

    def LoadOrder(self):
        self.LoadClient()
        self.LoadOrderItems()
        self.LoadAssignedEmployees()
        self.LoadPaymentInfo()

    def LoadPaymentInfo(self):
        orderID = self.cbOrder.currentText()

        # open connection
        connection = myconnutils.getprodconn()

        sql = "SELECT orderpayment.*, `order`.* " \
              "FROM orderpayment " \
              "inner join `order` on orderpayment.OrderID = `order`.OrderID " \
              "WHERE orderpayment.OrderID = '{0}'".format(orderID)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            for dic in rows:
                self.lblTotalPrice.setText(str(dic['OrderPriceTotal']))
                self.lblAmountDue.setText(str(dic['AmountDue']))
                self.lblLastPayment.setText(str(dic['LastPaymentDate']))
                self.lblCompleted.setText(str(dic['OrderDateCompleted']))
                self.lblInstalled.setText(str(dic['OrderInstallationDate']))

        finally:
            connection.close()

    def LoadAssignedEmployees(self):
        orderID = self.cbOrder.currentText()
        # open connection
        connection = myconnutils.getprodconn()

        sql = "SELECT assignedemployee.*, department.DepartmentName " \
              "from assignedemployee " \
              "inner join department on assignedemployee.DepartmentID = department.DepartmentID " \
              "WHERE assignedemployee.OrderID = '{0}'".format(orderID)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            removeRowIndex = self.tblAssignedEmployees.rowCount()
            for row in range(removeRowIndex):
                self.tblAssignedEmployees.removeRow(row)

            for rowIndex, dic in enumerate(rows):
                insertPosition = self.tblAssignedEmployees.rowCount()
                self.tblAssignedEmployees.insertRow(insertPosition)
                empName = QTableWidgetItem((dic['EmployeeFirstName']) + ' ' + str(dic['EmployeeLastName']))
                empDept = QTableWidgetItem(dic['DepartmentName'])

                self.tblAssignedEmployees.setItem(rowIndex, 0, empName)
                self.tblAssignedEmployees.setItem(rowIndex, 1, empDept)

        finally:
            connection.close()

    def LoadOrderItems(self):
        orderID = self.cbOrder.currentText()

        # open connection
        connection = myconnutils.getprodconn()

        sql = "SELECT orderitem.*, `order`.*, inventory.InventoryDescription " \
              "from orderitem " \
              "inner join `order` on orderitem.OrderID = `order`.OrderID " \
              "inner join inventory on orderitem.InventoryID = inventory.InventoryID " \
              "WHERE `order`.OrderID = '{0}'".format(orderID)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()

            removeRowIndex = self.tblOrderItem.rowCount()
            for row in range(removeRowIndex):
                self.tblOrderItem.removeRow(row)

            for rowIndex, dic in enumerate(rows):
                insertPosition = self.tblOrderItem.rowCount()
                self.tblOrderItem.insertRow(insertPosition)
                orderItem = QTableWidgetItem(dic['InventoryDescription'])
                itemPrice = QTableWidgetItem(str(dic['OrderItemPrice']))
                itemQuantity = QTableWidgetItem(str(dic['ItemQuantity']))

                self.tblOrderItem.setItem(rowIndex, 0, orderItem)
                self.tblOrderItem.setItem(rowIndex, 1, itemPrice)
                self.tblOrderItem.setItem(rowIndex, 2, itemQuantity)

        finally:
            connection.close()

    def LoadClient(self):
        orderID = self.cbOrder.currentText()

        # open connection
        connection = myconnutils.getprodconn()

        sql = "SELECT client.*, `order`.*, city.* " \
              "from client " \
                "inner join `order` on client.ClientID = `order`.ClientID " \
                "inner join city ON client.CityID = city.CityID " \
              "WHERE `order`.OrderID = '{0}'".format(orderID)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()
            for dic in rows:
                self.lblFirst.setText(dic['ClientFirstName'])
                self.lblLast.setText(dic['ClientLastName'])
                self.lblPhone.setText(dic['ClientPhoneNumber'])
                self.lblAddress.setText(dic['ClientAddress'])
                self.lblZipCode.setText(dic['ClientZipCode'])
                self.lblCity.setText(dic['CityName'])

        finally:
            connection.close()

    def SaveDeposit(self):
        orderID = self.cbOrder.currentText()

        newDeposit = self.lnDeposit.text()
        if orderID == '' or orderID is None:
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please choose an order ID from the dropdown list.')
            popup.exec_()
            return

        try:
            if newDeposit == '' or float(newDeposit) == 0.0:
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Add Order')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('Please a valid deposit amount.')
                popup.exec_()
                return

        except ValueError as error:
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Add Order')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please a valid deposit amount.')
            popup.exec_()
            return

        oldAmountDue = self.lblAmountDue.text()

        newAmountDue: float = float(oldAmountDue) - float(newDeposit)

        today = date.today()
        newPaymentDate = today.strftime("%Y-%m-%d")

        #initialize production database connection
        connection = myconnutils.getprodconn()

        sql = "SELECT Deposit " \
              "FROM orderpayment " \
              "WHERE orderpayment.OrderID = '{0}'".format(orderID)

        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

            rows = cursor.fetchall()
            rowdict = rows[0]
            oldDeposit = rowdict['Deposit']

            totalDeposited = oldDeposit + float(newDeposit)
            totalDepositRounded = round(totalDeposited, 2)
            newAmountDueRounded = round(newAmountDue, 2)
        finally:
            connection.close()

        valuesList = [orderID, newAmountDueRounded, totalDepositRounded, newPaymentDate]

        # initialize production database connection
        connection = myconnutils.getprodconn()

        sql1 = "UPDATE orderpayment SET AmountDue='{1}', Deposit='{2}', LastPaymentDate='{3}' " \
               "WHERE orderpayment.OrderID = '{0}'".format(*valuesList)

        try:
            cursor = connection.cursor()
            cursor.execute(sql1)
            connection.commit()

        finally:
            connection.close()

        self.LoadPaymentInfo()

        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Manage Orders')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Deposit saved succesfully!')
        popup.exec_()

    def addCompletedDate(self):
        orderID = self.cbOrder.currentText()

        if orderID == '' or orderID is None:
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please choose an order ID from the dropdown list.')
            popup.exec_()
            return

        dateCompleted = self.dateEdit.date()
        strCompDate = dateCompleted.toString('yyyy-MM-dd')

        if strCompDate == '2000-01-01':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter a completetion date.')
            popup.exec_()
            return

        valuesList = [orderID, strCompDate]
        # initialize production database connection
        connection = myconnutils.getprodconn()

        sql = "UPDATE `order` SET OrderDateCompleted = '{1}' " \
              "WHERE `order`.OrderID = '{0}'".format(*valuesList)
        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

        finally:
            connection.close()

        self.lblCompleted.setText(strCompDate)

        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Manage Orders')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Order completion date saved succesfully!')
        popup.exec_()

    def addInstalledDate(self):
        orderID = self.cbOrder.currentText()

        if orderID == '' or orderID is None:
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please choose an order ID from the dropdown list.')
            popup.exec_()
            return

        dateInstalled = self.dateEdit.date()
        strInstDate = dateInstalled.toString('yyyy-MM-dd')

        if strInstDate == '2000-01-01':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter an installation date.')
            popup.exec_()
            return

        valuesList = [orderID, strInstDate]
        # initialize production database connection
        connection = myconnutils.getprodconn()

        sql = "UPDATE `order` SET OrderInstallationDate = '{1}' " \
              "WHERE `order`.OrderID = '{0}'".format(*valuesList)
        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

        finally:
            connection.close()

        self.lblInstalled.setText(strInstDate)

        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Manage Orders')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Order installation date saved succesfully!')
        popup.exec_()


class EditEmployeeView(QtWidgets.QDialog):
    def __init__(self):
        super(EditEmployeeView, self).__init__()
        uic.loadUi('editemployeelist.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('Edit Employee List')
        self.btnClose.clicked.connect(self.close)
        self.btnSave.clicked.connect(self.SaveEmployees)
        self.btnAdd.clicked.connect(self.addEmployeeToList)
        self.btnDelete.clicked.connect(self.DeleteEmployee)
        self.popEmployeeTable()
        self.populateCityComboBox()
        self.populateDepartmentComboBox()


    def populateCityComboBox(self):
        #open connection to production database
        connection = myconnutils.getprodconn()
        sql = "select * from City ORDER BY CityName"

        try:
            # create cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                cityName = record['CityName']
                self.cbCity.addItem(cityName)

        finally:
            # close connection
            connection.close()

    def populateDepartmentComboBox(self):
        # open connection to payroll database
        connection = myconnutils.getpayconn()
        sql = "select DepartmentName from department ORDER BY DepartmentName"

        try:
            # create cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                depName = record['DepartmentName']
                self.cbDepartment.addItem(depName)

        finally:
            # close connection
            connection.close()

    def popEmployeeTable(self):
        # get number of rows in table

        # open connection to payroll database
        connection = myconnutils.getpayconn()
        sql = "select employee.*, department.*, city.* " \
              "from employee " \
                "inner join department on employee.DepartmentID = department.DepartmentID " \
                "inner join city on employee.CityID = city.CityID " \
              "ORDER BY EmployeeName"

        try:
            # create cursor and execute SQL
            cursor =  connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                rowCount = self.tblEmpTable.rowCount()
                self.tblEmpTable.insertRow(rowCount)
                empFirst = QTableWidgetItem(record['EmployeeName'])
                empLast = QTableWidgetItem(record['EmployeeLastName'])
                empDept = QTableWidgetItem(record['DepartmentName'])
                empAddress = QTableWidgetItem(record['EmployeeAddress'])
                empCity = QTableWidgetItem(record['CityName'])
                empPhone = QTableWidgetItem(record['EmployeePhoneNumber'])
                empPosition = QTableWidgetItem(record['EmployeePosition'])
                empHourlyRate = QTableWidgetItem(record['HourlyRate'])

                self.tblEmpTable.setItem(rowIndex, 0, empFirst)
                self.tblEmpTable.setItem(rowIndex, 1, empLast)
                self.tblEmpTable.setItem(rowIndex, 2, empDept)
                self.tblEmpTable.setItem(rowIndex, 3, empAddress)
                self.tblEmpTable.setItem(rowIndex, 4, empCity)
                self.tblEmpTable.setItem(rowIndex, 5, empPhone)
                self.tblEmpTable.setItem(rowIndex, 6, empPosition)
                self.tblEmpTable.setItem(rowIndex, 7, empHourlyRate)

        finally:
            # close connection
            connection.close()

    def addEmployeeToList(self):
        firstName = self.lnFirst.text()
        lastName = self.lnLast.text()
        empCity = self.cbCity.currentText()
        address = self.lnAddress.text()
        department = self.cbDepartment.currentText()
        phone = self.lnPhone.text()
        position = self.cbPosition.currentText()
        rate = self.lnRate.text()


        if firstName == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter a first name.')
            popup.exec_()

        elif lastName == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter a last name.')
            popup.exec_()

        elif empCity == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please choose a city.')
            popup.exec_()

        elif address == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter an address.')
            popup.exec_()

        elif department == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please choose a department.')
            popup.exec_()

        elif phone == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter a phone number.')
            popup.exec_()

        elif position == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please choose a position for the employee.')
            popup.exec_()

        elif rate == '':
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Please enter an hourly pay rate for the employee.')
            popup.exec_()

        else:
            rowPosition = self.tblEmpTable.rowCount()
            self.tblEmpTable.insertRow(rowPosition)
            # insert content of text and combo boxes into last row of table
            self.tblEmpTable.setItem(rowPosition, 0, QTableWidgetItem(firstName))
            self.tblEmpTable.setItem(rowPosition, 1, QTableWidgetItem(lastName))
            self.tblEmpTable.setItem(rowPosition, 2, QTableWidgetItem(department))
            self.tblEmpTable.setItem(rowPosition, 3, QTableWidgetItem(address))
            self.tblEmpTable.setItem(rowPosition, 4, QTableWidgetItem(empCity))
            self.tblEmpTable.setItem(rowPosition, 5, QTableWidgetItem(phone))
            self.tblEmpTable.setItem(rowPosition, 6, QTableWidgetItem(position))
            self.tblEmpTable.setItem(rowPosition, 7, QTableWidgetItem(rate))

    def SaveEmployees(self):
        # open connection to production database
        connection = myconnutils.getpayconn()

        # do query to fetch employee table column names
        sql = "SELECT * FROM employee WHERE 1=0;"

        # create cursor and execute sql
        cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()

        rows = cursor.fetchall()  # this is your list of dictionaries

        colList = []
        # populate list with column titles
        for i in range(1, 8):
            desc = cursor.description[i]
            colList.append(desc[0])

        # iterate through table values until it finds an empty field
        allRows = self.tblEmpTable.rowCount()
        allColumns = self.tblEmpTable.columnCount()

        for row in range(allRows):
            valuesList = []
            valuesList.append(row+1)
            if self.tblEmpTable.item(row, 0) is not None:
                for column in range(allColumns):
                    if self.tblEmpTable.item(row, column) is not None:
                        cellContent = self.tblEmpTable.item(row, column).text()
                        valuesList.append(cellContent)

                    else:
                        # shows message box about error
                        popup = QtWidgets.QMessageBox()
                        popup.setWindowTitle('Error')
                        icon = QtGui.QIcon("./img/mangotech.jpg")
                        popup.setWindowIcon(icon)
                        popup.setText(
                            'Please do not leave any empty fields when entering a new record.')
                        popup.exec_()
                        return

                try:
                    sql = "REPLACE INTO employee VALUES ('{0}', '{2}', '{1}', '{4}', (SELECT CityID from city WHERE CityName = '{5}'), '{6}', (SELECT DepartmentID from department WHERE DepartmentName = '{3}'), '{7}', '{8}')".format(*valuesList)
                    sql1 = "REPLACE INTO salary (EmployeeID) VALUES ('{0}')".format(*valuesList)

                except IndexError as error:
                    # shows message box about error
                    popup = QtWidgets.QMessageBox()
                    popup.setWindowTitle('Error')
                    icon = QtGui.QIcon("./img/mangotech.jpg")
                    popup.setWindowIcon(icon)
                    popup.setText('Last row not saved. Please enter a value '
                                  'in every field when entering a new record.')
                    popup.exec_()
                    return

                else:
                    if cellContent == '':
                        # shows message box about error
                        popup = QtWidgets.QMessageBox()
                        popup.setWindowTitle('Error')
                        icon = QtGui.QIcon("./img/mangotech.jpg")
                        popup.setWindowIcon(icon)
                        popup.setText(
                            'Please do not leave any fields blank when entering a new record.')
                        popup.exec_()
                        return

                try:
                    # create cursor and execute sql to save employee table
                    connection = myconnutils.getpayconn()
                    cursor = connection.cursor()
                    cursor.execute(sql)
                    connection.commit()

                finally:
                    # close connection
                    connection.close()

                try:
                    # create cursor and execute sql to create employee entry in salary table
                    connection = myconnutils.getpayconn()
                    cursor = connection.cursor()
                    # cursor.execute(sql1)
                    connection.commit()

                finally:
                    # close connection
                    connection.close()

        # shows message box saying employee was succesfully updated.
        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Edit Employee List')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Employee list updated succesfully!')
        popup.exec_()

    def DeleteEmployee(self):
        selectedItems = self.tblEmpTable.selectedItems()
        stringItemList= []

        for i in selectedItems:
            stringItemList.append(i.text())

        if stringItemList == [] or len(stringItemList) == 0:
            # shows message box about error
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Error')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Cannot delete an empty row.')
            popup.exec_()

        else:
            # open connection to payroll database
            connection = myconnutils.getpayconn()

            sql = "DELETE FROM employee WHERE EmployeeLastName = '{1}' AND EmployeeName = '{0}' AND EmployeeAddress = '{3}' AND " \
                  "EmployeePhoneNumber = '{5}' AND EmployeePosition = '{6}'".format(*stringItemList)

            try:
                # create cursor and execute sql
                cursor = connection.cursor()
                cursor.execute(sql)
                connection.commit()

            finally:
                # close connection
                connection.close()

            # delete the selected row from the tablewidget
            currentRow = self.tblEmpTable.currentRow()
            self.tblEmpTable.removeRow(currentRow)

            # shows message box saying employee was succesfully deleted.
            popup = QtWidgets.QMessageBox()
            popup.setWindowTitle('Edit Employee List')
            icon = QtGui.QIcon("./img/mangotech.jpg")
            popup.setWindowIcon(icon)
            popup.setText('Employee list updated succesfully!')
            popup.exec_()


class PayrollView(QtWidgets.QDialog):
    def __init__(self):
        super(PayrollView, self).__init__()
        uic.loadUi('payroll.ui', self)
        # focuswindow
        self.isActiveWindow()
        self.setWindowTitle('Employee Payroll')
        self.btnClose.clicked.connect(self.close)
        self.btnCalculate.clicked.connect(self.CalculatePayroll)
        self.btnPay.clicked.connect(self.PayEmployees)
        self.popPayrollTable()

    def popPayrollTable(self):
        # open connection to payroll database
        connection = myconnutils.getpayconn()
        sql = "select employee.*, department.DepartmentName, salary.* " \
              "from employee " \
                "inner join department on employee.DepartmentID = department.DepartmentID " \
                "left join salary on employee.EmployeeID = salary.EmployeeID " \
              "WHERE MONTH(LastPaymentDate) <> MONTH(CURRENT_DATE()) OR LastPaymentDate IS NULL " \
              "ORDER BY EmployeeName;"

        try:
            # create a cursor and execute SQL
            cursor = connection.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()  # this is your list of dictionaries

            for rowIndex, record in enumerate(rows):
                self.tblPayTable.insertRow(rowIndex)
                empFirst = QTableWidgetItem(record['EmployeeName']).text()
                empLast = QTableWidgetItem(record['EmployeeLastName']).text()
                empName = QTableWidgetItem(empFirst + ' ' + empLast)
                empDept = QTableWidgetItem(record['DepartmentName'])
                empPos = QTableWidgetItem(record['EmployeePosition'])


                strHours = str(record['HoursWorkedInPeriod'])
                empHours = QTableWidgetItem(strHours)

                strTotal = str(record['TotalSalary'])
                empTotal = QTableWidgetItem(strTotal)

                strDate = str(record['LastPaymentDate'])

                if strDate == 'None':
                    empLastPay = QTableWidgetItem(strDate)
                else:
                    empLastPay = QTableWidgetItem(strDate)

                strRate = str(record['HourlyRate'])
                empRate = QTableWidgetItem(strRate)

                self.tblPayTable.setItem(rowIndex, 0, empName)
                self.tblPayTable.setItem(rowIndex, 1, empDept)
                self.tblPayTable.setItem(rowIndex, 2, empPos)
                self.tblPayTable.setItem(rowIndex, 3, empRate)
                self.tblPayTable.setItem(rowIndex, 4, empHours)
                self.tblPayTable.setItem(rowIndex, 5, empTotal)
                self.tblPayTable.setItem(rowIndex, 6, empLastPay)

        finally:
            # close connection
            connection.close()

    def CalculatePayroll(self):
        allRows = self.tblPayTable.rowCount()
        allColumns = self.tblPayTable.columnCount()
        hoursValues = []

        for row in range(allRows):
            if self.tblPayTable.item(row, 4) is not None and self.tblPayTable.item(row, 4).text() != 'None':
                hours = self.tblPayTable.item(row, 4).text()
                if hours.strip() == '':
                    # shows error message box
                    popup = QtWidgets.QMessageBox()
                    popup.setWindowTitle('Error')
                    icon = QtGui.QIcon("./img/mangotech.jpg")
                    popup.setWindowIcon(icon)
                    popup.setText('Please enter a valid Hours Worked value.')
                    popup.exec_()
                    break
                else:
                    time = float(self.tblPayTable.item(row, 4).text())
                    rate = self.tblPayTable.item(row, 3).text()
                    payposdict = {"rate" : rate, "hours" : time}
                    hoursValues.append(payposdict)
            else:
                # shows error message box
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Error')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('Please enter a valid Hours Worked value.')
                popup.exec_()
                break

        for dictIndex, dict in enumerate(hoursValues):
            intHours = float(dict['hours'])
            dblRate = float(dict['rate'].strip('$'))
            strHours = intHours * dblRate
            dictItem = QTableWidgetItem('$' + str(round(strHours, 2)))
            self.tblPayTable.setItem(dictIndex, 5, dictItem)

    def PayEmployees(self):
        today = date.today()
        strDate = today.strftime("%Y-%m-%d")

        # get row count to iterate through rows
        allRows = self.tblPayTable.rowCount()

        for row in range(allRows):
            if self.tblPayTable.item(row, 4) is not None:
                if self.tblPayTable.item(row, 4).text() == '':
                    # shows error message box
                    popup = QtWidgets.QMessageBox()
                    popup.setWindowTitle('Error')
                    icon = QtGui.QIcon("./img/mangotech.jpg")
                    popup.setWindowIcon(icon)
                    popup.setText('Please enter a valid Hours Worked value.')
                    popup.exec_()
                    break

                else:
                    itemDate = QTableWidgetItem(strDate)
                    self.tblPayTable.setItem(row, 6, itemDate)

                    # open connection
                    connection = myconnutils.getpayconn()

                    # do query to save payroll table
                    valuesList = []
                    valuesList.append(row + 1)
                    for column in range(0, 7):
                        cellContent = self.tblPayTable.item(row, column).text()
                        valuesList.append(cellContent)
                    fullName = valuesList[1]

                    try:
                        sql = "SELECT EmployeeID FROM employee WHERE EmployeeName='{0}' AND EmployeeLastName='{1}'".format(*fullName.split())
                    finally:
                        # create cursor and execute SQL to get employee IDs
                        cursor = connection.cursor()
                        cursor.execute(sql)
                        connection.commit()

                    rows = cursor.fetchall() # this is your list of dictionaries

                    # replace the row index for the employeeID
                    rowdict = rows[0]
                    empID = rowdict['EmployeeID']
                    del valuesList[0]
                    valuesList.insert(0, empID)

                    try:
                        # sql1 = "INSERT INTO salary (EmployeeID, HoursWorkedInPeriod, TotalSalary, LastPaymentDate) VALUES ('{0}', '{5}', '{6}', '{7}')".format(*valuesList)
                        sql1 = "UPDATE salary SET HoursWorkedInPeriod='{5}', TotalSalary='{6}', LastPaymentDate='{7}' WHERE EmployeeID='{0}'".format(*valuesList)

                    finally:
                        # create cursor and execute sql to save inventory table
                        cursor = connection.cursor()
                        cursor.execute(sql1)
                        connection.commit()

            else:
                # shows error message box
                popup = QtWidgets.QMessageBox()
                popup.setWindowTitle('Error')
                icon = QtGui.QIcon("./img/mangotech.jpg")
                popup.setWindowIcon(icon)
                popup.setText('Please enter a valid Hours Worked value.')
                popup.exec_()
                break

        # close connection
        connection.close()

        # shows message box saying payroll was succesfully udpated
        popup = QtWidgets.QMessageBox()
        popup.setWindowTitle('Manage Payroll')
        icon = QtGui.QIcon("./img/mangotech.jpg")
        popup.setWindowIcon(icon)
        popup.setText('Payroll successfully saved!')
        popup.exec_()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    # show main window
    window = MainView()
    window.show()
    sys.exit(app.exec_())
