import pymysql.cursors


# Function return a connection.
def getprodconn():
    # You can change the connection arguments.
    connection = pymysql.connect(host='127.0.0.1',
                                 user='root',
                                 password='root',
                                 db='production',
                                 charset='latin1',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection


def getpayconn():
    # You can change the connection arguments.
    connection = pymysql.connect(host='127.0.0.1',
                                 user='root',
                                 password='root',
                                 db='payroll',
                                 charset='latin1',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection
