-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.15 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for production
CREATE DATABASE IF NOT EXISTS `production` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `production`;

-- Dumping structure for table production.assigned-employee
CREATE TABLE IF NOT EXISTS `assigned-employee` (
  `AssignedEmployeeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(10) unsigned,
  `EmployeeLastName` varchar(50) DEFAULT NULL,
  `EmployeeFirstName` varchar(50) DEFAULT NULL,
  `DepartmentID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`AssignedEmployeeID`),
  KEY `FK_assigned-employee_order` (`OrderID`),
  KEY `FK_assigned-employee_department` (`DepartmentID`),
  CONSTRAINT `FK_assigned-employee_department` FOREIGN KEY (`DepartmentID`) REFERENCES `department` (`DepartmentID`),
  CONSTRAINT `FK_assigned-employee_order` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table production.assigned-employee: ~0 rows (approximately)
/*!40000 ALTER TABLE `assigned-employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `assigned-employee` ENABLE KEYS */;

-- Dumping structure for table production.city
CREATE TABLE IF NOT EXISTS `city` (
  `CityID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CityName` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CityID`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- Dumping data for table production.city: ~2 rows (approximately)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` (`CityID`, `CityName`) VALUES
	(1, 'Adjuntas'),
	(2, 'Aguada'),
	(3, 'Aguadilla'),
	(4, 'Aguas Buenas'),
	(5, 'Aibonito'),
	(6, 'Arecibo'),
	(7, 'Arroyo'),
	(8, 'Añasco'),
	(9, 'Barceloneta'),
	(10, 'Barranquitas'),
	(11, 'Bayamon'),
	(12, 'Cabo Rojo'),
	(13, 'Caguas'),
	(14, 'Camuy'),
	(15, 'Canovanas'),
	(16, 'Carolina'),
	(17, 'Cataño'),
	(18, 'Cayey'),
	(19, 'Ceiba'),
	(20, 'Ciales'),
	(21, 'Cidra'),
	(22, 'Coamo'),
	(23, 'Comerío'),
	(24, 'Corozal'),
	(25, 'Culebra'),
	(26, 'Dorado'),
	(27, 'Fajardo'),
	(28, 'Florida'),
	(29, 'Guayama'),
	(30, 'Guayanilla'),
	(31, 'Guaynabo'),
	(32, 'Gurabo'),
	(33, 'Guánica'),
	(34, 'Hatillo'),
	(35, 'Hormigueros'),
	(36, 'Humacao'),
	(37, 'Isabela'),
	(38, 'Jayuya'),
	(39, 'Juana Diaz'),
	(40, 'Juncos'),
	(41, 'Lajas'),
	(42, 'Lares'),
	(43, 'Las Marias'),
	(44, 'Las Piedras'),
	(45, 'Loiza'),
	(46, 'Luquillo'),
	(47, 'Manatí'),
	(48, 'Maricao'),
	(49, 'Maunabo'),
	(50, 'Mayagüez'),
	(51, 'Moca'),
	(52, 'Morovis'),
	(53, 'Naguabo'),
	(54, 'Naranjito'),
	(55, 'Orocovis'),
	(56, 'Patillas'),
	(57, 'Peñuelas'),
	(58, 'Ponce'),
	(59, 'Quebradillas'),
	(60, 'Rincon'),
	(61, 'Rio Grande'),
	(62, 'Sabana Grande'),
	(63, 'Salinas'),
	(64, 'San Germán'),
	(65, 'San Juan'),
	(66, 'San Lorenzo'),
	(67, 'San Sebastian'),
	(68, 'Santa Isabel'),
	(69, 'Toa Alta'),
	(70, 'Toa Baja'),
	(71, 'Trujillo Alto'),
	(72, 'Utuado'),
	(73, 'Vega Alta'),
	(74, 'Vega Baja'),
	(75, 'Vieques'),
	(76, 'Villalba'),
	(77, 'Yabucoa'),
	(78, 'Yauco');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

-- Dumping structure for table production.client
CREATE TABLE IF NOT EXISTS `client` (
  `ClientID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClientLastName` char(50) NOT NULL DEFAULT '0',
  `ClientFirstName` char(50) NOT NULL DEFAULT '0',
  `ClientPhoneNumber` varchar(50) NOT NULL DEFAULT '0',
  `ClientAddress` varchar(50) NOT NULL DEFAULT '0',
  `CityID` int(11) unsigned NOT NULL DEFAULT '0',
  `ClientZipCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ClientID`),
  KEY `FK_client_city` (`CityID`),
  CONSTRAINT `FK_client_city` FOREIGN KEY (`CityID`) REFERENCES `city` (`CityID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table production.client: ~2 rows (approximately)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Dumping structure for table production.department
CREATE TABLE IF NOT EXISTS `department` (
  `DepartmentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DepartmentName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DepartmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table production.department: ~6 rows (approximately)
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`DepartmentID`, `DepartmentName`) VALUES
	(1, 'Raw Materials'),
	(2, 'Cutting'),
	(3, 'Painting'),
	(4, 'Design'),
	(5, 'Glasswork'),
	(6, 'Installation'),
	(7, 'Assembly');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;

-- Dumping structure for table production.inventory
CREATE TABLE IF NOT EXISTS `inventory` (
  `InventoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `InventoryDescription` varchar(50) NOT NULL DEFAULT '0',
  `InventoryStock` varchar(50) NOT NULL DEFAULT '0',
  `InventoryCost` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`InventoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table production.inventory: ~14 rows (approximately)
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;

-- Dumping structure for table production.order
CREATE TABLE IF NOT EXISTS `order` (
  `OrderID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClientID` int(10) unsigned NOT NULL,
  `OrderPriceTotal` double unsigned NOT NULL,
  `OrderPlacedDate` date NOT NULL,
  `OrderDateCompleted` date DEFAULT NULL,
  `OrderInstallationDate` date DEFAULT NULL,
  PRIMARY KEY (`OrderID`),
  KEY `FK1lol` (`ClientID`),
  CONSTRAINT `FK1lol` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table production.order: ~0 rows (approximately)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for table production.order-item
CREATE TABLE IF NOT EXISTS `order-item` (
  `OrderItemID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(10) unsigned NOT NULL DEFAULT '0',
  `InventoryID` int(10) unsigned NOT NULL DEFAULT '0',
  `OrderItemPrice` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`OrderItemID`),
  KEY `FK_order-item_order` (`OrderID`),
  KEY `FK_order-item_inventory` (`InventoryID`),
  CONSTRAINT `FK_order-item_inventory` FOREIGN KEY (`InventoryID`) REFERENCES `inventory` (`InventoryID`),
  CONSTRAINT `FK_order-item_order` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table production.order-item: ~0 rows (approximately)
/*!40000 ALTER TABLE `order-item` DISABLE KEYS */;
/*!40000 ALTER TABLE `order-item` ENABLE KEYS */;

-- Dumping structure for table production.order-payment
CREATE TABLE IF NOT EXISTS `order-payment` (
  `PaymentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(10) unsigned NOT NULL,
  `AmountDue` double DEFAULT NULL,
  `Deposit` double NOT NULL,
  `LastPaymentDate` date NOT NULL,
  PRIMARY KEY (`PaymentID`),
  KEY `OrderID` (`OrderID`),
  CONSTRAINT `FK_client-payment_order` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table production.order-payment: ~0 rows (approximately)
/*!40000 ALTER TABLE `order-payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `order-payment` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
