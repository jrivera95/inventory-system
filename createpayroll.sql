-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.15 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for payroll
CREATE DATABASE IF NOT EXISTS `payroll` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `payroll`;

-- Dumping structure for table payroll.city
CREATE TABLE IF NOT EXISTS `city` (
  `CityID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CityName` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CityID`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- Dumping data for table payroll.city: ~2 rows (approximately)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` (`CityID`, `CityName`) VALUES
	(1, 'Adjuntas'),
	(2, 'Aguada'),
	(3, 'Aguadilla'),
	(4, 'Aguas Buenas'),
	(5, 'Aibonito'),
	(6, 'Arecibo'),
	(7, 'Arroyo'),
	(8, 'Añasco'),
	(9, 'Barceloneta'),
	(10, 'Barranquitas'),
	(11, 'Bayamon'),
	(12, 'Cabo Rojo'),
	(13, 'Caguas'),
	(14, 'Camuy'),
	(15, 'Canovanas'),
	(16, 'Carolina'),
	(17, 'Cataño'),
	(18, 'Cayey'),
	(19, 'Ceiba'),
	(20, 'Ciales'),
	(21, 'Cidra'),
	(22, 'Coamo'),
	(23, 'Comerío'),
	(24, 'Corozal'),
	(25, 'Culebra'),
	(26, 'Dorado'),
	(27, 'Fajardo'),
	(28, 'Florida'),
	(29, 'Guayama'),
	(30, 'Guayanilla'),
	(31, 'Guaynabo'),
	(32, 'Gurabo'),
	(33, 'Guánica'),
	(34, 'Hatillo'),
	(35, 'Hormigueros'),
	(36, 'Humacao'),
	(37, 'Isabela'),
	(38, 'Jayuya'),
	(39, 'Juana Diaz'),
	(40, 'Juncos'),
	(41, 'Lajas'),
	(42, 'Lares'),
	(43, 'Las Marias'),
	(44, 'Las Piedras'),
	(45, 'Loiza'),
	(46, 'Luquillo'),
	(47, 'Manatí'),
	(48, 'Maricao'),
	(49, 'Maunabo'),
	(50, 'Mayagüez'),
	(51, 'Moca'),
	(52, 'Morovis'),
	(53, 'Naguabo'),
	(54, 'Naranjito'),
	(55, 'Orocovis'),
	(56, 'Patillas'),
	(57, 'Peñuelas'),
	(58, 'Ponce'),
	(59, 'Quebradillas'),
	(60, 'Rincon'),
	(61, 'Rio Grande'),
	(62, 'Sabana Grande'),
	(63, 'Salinas'),
	(64, 'San Germán'),
	(65, 'San Juan'),
	(66, 'San Lorenzo'),
	(67, 'San Sebastian'),
	(68, 'Santa Isabel'),
	(69, 'Toa Alta'),
	(70, 'Toa Baja'),
	(71, 'Trujillo Alto'),
	(72, 'Utuado'),
	(73, 'Vega Alta'),
	(74, 'Vega Baja'),
	(75, 'Vieques'),
	(76, 'Villalba'),
	(77, 'Yabucoa'),
	(78, 'Yauco');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

-- Dumping structure for table payroll.department
CREATE TABLE IF NOT EXISTS `department` (
  `DepartmentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DepartmentName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DepartmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table payroll.department: ~6 rows (approximately)
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`DepartmentID`, `DepartmentName`) VALUES
	(1, 'Raw Materials'),
	(2, 'Design'),
	(3, 'Cutting'),
	(4, 'Glasswork'),
	(5, 'Assembly'),
	(6, 'Painting'),
	(7, 'Installation');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;

-- Dumping structure for table payroll.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `EmployeeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EmployeeLastName` varchar(50) NOT NULL DEFAULT '0',
  `EmployeeName` varchar(50) NOT NULL DEFAULT '0',
  `EmployeeAddress` varchar(50) NOT NULL DEFAULT '0',
  `CityID` int(10) unsigned NOT NULL DEFAULT '0',
  `EmployeePhoneNumber` varchar(50) NOT NULL DEFAULT '0',
  `DepartmentID` int(10) unsigned NOT NULL DEFAULT '0',
  `EmployeePosition` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`EmployeeID`),
  KEY `FK_emlpoyee_city` (`CityID`),
  KEY `FK_emlpoyee_department` (`DepartmentID`),
  CONSTRAINT `FK_emlpoyee_city` FOREIGN KEY (`CityID`) REFERENCES `city` (`CityID`),
  CONSTRAINT `FK_emlpoyee_department` FOREIGN KEY (`DepartmentID`) REFERENCES `department` (`DepartmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table payroll.employee: ~5 rows (approximately)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

-- Dumping structure for table payroll.salary
CREATE TABLE IF NOT EXISTS `salary` (
  `EmployeeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LastPaymentDate` date DEFAULT NULL,
  `HoursWorkedInPeriod` int(11) DEFAULT '0',
  `TotalSalary` double DEFAULT '0',
  PRIMARY KEY (`EmployeeID`),
  CONSTRAINT `FK_Employee-Salary_emlpoyee` FOREIGN KEY (`EmployeeID`) REFERENCES `employee` (`EmployeeID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table payroll.salary: ~5 rows (approximately)
/*!40000 ALTER TABLE `salary` DISABLE KEYS */;
/*!40000 ALTER TABLE `salary` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
